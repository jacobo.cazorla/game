package com.casa.tiendaJuegos.app.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.casa.tiendaJuegos.app.converter.DtoToEntityConverter;
import com.casa.tiendaJuegos.app.converter.EntityToDtoConverter;
import com.casa.tiendaJuegos.app.converter.EntityToGeneroResponseConverter;
import com.casa.tiendaJuegos.app.converter.FromEnumConverter;
import com.casa.tiendaJuegos.app.converter.GeneroRequestToEntityConverter;
import com.casa.tiendaJuegos.app.converter.GeneroResponseToEntityConverter;
import com.casa.tiendaJuegos.app.converter.ShopDtoToShopEntityConverter;
import com.casa.tiendaJuegos.app.converter.ShopEntityToShopDtoConverter;
import com.casa.tiendaJuegos.app.converter.StockDtoToStockEntityConverter;
import com.casa.tiendaJuegos.app.converter.StockEntityToStockDtoConverter;

@Configuration
public class WebConfig implements WebMvcConfigurer {
 
    @Override
    public void addFormatters(FormatterRegistry registry) {
    	
        registry.addConverter(new DtoToEntityConverter());
        registry.addConverter(new EntityToDtoConverter());
        registry.addConverter(new FromEnumConverter());   
        registry.addConverter(new GeneroRequestToEntityConverter());
        registry.addConverter(new EntityToGeneroResponseConverter());
        registry.addConverter(new GeneroResponseToEntityConverter());
        registry.addConverter(new ShopDtoToShopEntityConverter());
        registry.addConverter(new ShopEntityToShopDtoConverter());
        registry.addConverter(new StockDtoToStockEntityConverter());
        registry.addConverter(new StockEntityToStockDtoConverter());
    }
}
