package com.casa.tiendaJuegos.app.controllers;



import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.casa.tiendaJuegos.app.dtos.request.ShopRequest;
import com.casa.tiendaJuegos.app.services.ShopService;

@RestController
public class ShopController {
	
	@Autowired
	ShopService shopService;


	@PostMapping(path="/shop", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> addShop(@Valid @RequestBody ShopRequest shop) {

		shopService.addShop(shop);
		
		return ResponseEntity.status(HttpStatus.OK).body(shop);
	}
	
	@GetMapping("/shop/{id}")
	public ResponseEntity<Object> getShop(@PathVariable Long id) {
		return ResponseEntity.status(HttpStatus.OK).body(shopService.getShop(id)); 
	}	
	
}
