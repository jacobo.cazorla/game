package com.casa.tiendaJuegos.app.controllers;



import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.casa.tiendaJuegos.app.dtos.request.GameRequest;
import com.casa.tiendaJuegos.app.services.JuegoService;

@RestController
public class JuegoController {
	
	@Autowired
	JuegoService juegoService;


	@PostMapping(path="/juego", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> addJuego(@Valid @RequestBody GameRequest juego) {

		juegoService.addJuego(juego);
		
		return ResponseEntity.status(HttpStatus.OK).body(juego);
	}
	
	@GetMapping("/juego/{titulo}")
	public ResponseEntity<Object> getJuego(@PathVariable String titulo) {
		return ResponseEntity.status(HttpStatus.OK).body(juegoService.getJuego(titulo)); 
	}	
	
	@DeleteMapping("/juego/{titulo}")
	public ResponseEntity<Object> deleteJuego(@PathVariable String titulo) {
		juegoService.deleteJuego(titulo);
	    return ResponseEntity.status(HttpStatus.OK).body("Juego borrado correctamente");
	}
	
	@PutMapping("/juego/{titulo}")
	public ResponseEntity<Object> updateJuego(@RequestBody GameRequest juego, @PathVariable String titulo) {

		juegoService.updateJuego(juego,titulo);
		
		return ResponseEntity.status(HttpStatus.OK).body(juego);
	}

}
