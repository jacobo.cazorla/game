package com.casa.tiendaJuegos.app.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.casa.tiendaJuegos.app.dtos.request.StockRequest;
import com.casa.tiendaJuegos.app.services.StockService;

@RestController
public class StockController {
	
	@Autowired
	StockService stockService;


	@PostMapping(path="/stock", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> addStock(@Valid @RequestBody StockRequest stock) {

		stockService.addStock(stock);
		
		return ResponseEntity.status(HttpStatus.OK).body(stock);
	}
		
	@DeleteMapping("/stock/{codigo}")
	public ResponseEntity<Object> deleteStock(@PathVariable String codigo) {
		stockService.removeStock(codigo);
	    return ResponseEntity.status(HttpStatus.OK).body("Stock borrado correctamente");
	}
	
}
