package com.casa.tiendaJuegos.app.services;

import com.casa.tiendaJuegos.app.dtos.request.GameRequest;
import com.casa.tiendaJuegos.app.dtos.response.GameResponse;

public interface JuegoService {

	public GameResponse addJuego (GameRequest juego);
	public GameResponse getJuego(String titulo);
	public void deleteJuego(String juego);
	public GameResponse updateJuego (GameRequest juego,String titulo);
}
