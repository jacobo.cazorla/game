package com.casa.tiendaJuegos.app.services.Impl;

import com.casa.tiendaJuegos.app.dtos.request.GeneroRequest;
import com.casa.tiendaJuegos.app.dtos.response.GeneroResponse;
import com.casa.tiendaJuegos.app.entities.Genero;
import com.casa.tiendaJuegos.app.helper.GeneroHelper;
import com.casa.tiendaJuegos.app.repository.GeneroRepository;
import com.casa.tiendaJuegos.app.services.GeneroService;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

@Service
public class GeneroServiceImpl implements GeneroService {

	@Autowired
	private GeneroRepository generoRepo;
	
	@Autowired
	GeneroHelper generoHelper;
	
	@Autowired
	ConversionService converter;

	@Override
	public void addGenero(GeneroRequest gen) {
		generoRepo.save(converter.convert(gen,Genero.class));		
	}

	@Override
	public GeneroResponse getGenero(String gen) {
		Genero genero = generoHelper.getIfExistsGenero(generoRepo.findByDescription(gen));
		return converter.convert(genero,GeneroResponse.class);
	}

}
