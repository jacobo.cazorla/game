package com.casa.tiendaJuegos.app.services.Impl;

import com.casa.tiendaJuegos.app.dtos.request.StockRequest;
import com.casa.tiendaJuegos.app.dtos.response.StockResponse;
import com.casa.tiendaJuegos.app.entities.Game;
import com.casa.tiendaJuegos.app.entities.Shop;
import com.casa.tiendaJuegos.app.entities.Stock;
import com.casa.tiendaJuegos.app.entities.Tienda_Game_Stock;

import com.casa.tiendaJuegos.app.helper.JuegoHelper;
import com.casa.tiendaJuegos.app.helper.ShopHelper;
import com.casa.tiendaJuegos.app.helper.StockHelper;
import com.casa.tiendaJuegos.app.repository.GameRepository;
import com.casa.tiendaJuegos.app.repository.ShopRepository;
import com.casa.tiendaJuegos.app.repository.StockRepository;
import com.casa.tiendaJuegos.app.repository.Tienda_Game_StockRepository;
import com.casa.tiendaJuegos.app.services.StockService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

@Service
public class StockServiceImpl implements StockService {

	@Autowired
	private StockRepository stockRepo;

	@Autowired
	private GameRepository gameRepo;
	
	@Autowired
	private ShopRepository shopRepo;
	
	@Autowired
	private Tienda_Game_StockRepository tiendaGameStockRepo;
	
	@Autowired
	StockHelper stockHelper;

	@Autowired
	JuegoHelper gameHelper;

	@Autowired
	ShopHelper shopHelper;
	
	@Autowired
	ConversionService converter;
/*
	@Override
	public ShopResponse getShop(Long id) {
		//Buscar el juego por el título
		Shop shop = shopHelper.getIfExistsShop(shopRepo.findById(id).get());

		//Preparar la respuesta
		ShopResponse response = converter.convert(shop,ShopResponse.class);

		return response;
	}*/

	@Override
	public StockResponse addStock(StockRequest stockDto) {

		//Aplicar los converters
		Stock stock = converter.convert(stockDto,Stock.class);
		
		//Obtener las entidades game y shop
		Game game = gameHelper.getIfExistsGame(gameRepo.findById(stockDto.getId_game()).get());
		Shop shop = shopHelper.getIfExistsShop(shopRepo.findById(stockDto.getId_shop()).get());
		
		//Guardar en base de datos
		stock = stockRepo.save(stock);
		
		//Actualizar las entidades relacionadas
		game.getStock().add(stock);
		gameRepo.save(game);
		shop.getStock().add(stock);
		shopRepo.save(shop);
		
		Tienda_Game_Stock tiendaGameStock = new Tienda_Game_Stock();
		tiendaGameStock.setGame(game);
		tiendaGameStock.setStock(stock);
		tiendaGameStock.setShop(shop);
		
		tiendaGameStockRepo.save(tiendaGameStock);
		
		//Preparar la respuesta
		StockResponse response = converter.convert(stock, StockResponse.class);
		
		return response;
	}

	@Override
	public void removeStock(String codigo) {
		//Buscar el stock por el código
		Stock stock = stockHelper.getIfExistsStock(stockRepo.findByCodigo(codigo));
		
		stockRepo.delete(stock);
	}
}
