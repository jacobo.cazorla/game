package com.casa.tiendaJuegos.app.services;

import com.casa.tiendaJuegos.app.dtos.request.StockRequest;
import com.casa.tiendaJuegos.app.dtos.response.StockResponse;

public interface StockService {

	public StockResponse addStock (StockRequest stock);
	public void removeStock(String codigo);

}
