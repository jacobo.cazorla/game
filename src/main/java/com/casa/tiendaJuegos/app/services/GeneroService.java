package com.casa.tiendaJuegos.app.services;

import com.casa.tiendaJuegos.app.dtos.request.GeneroRequest;
import com.casa.tiendaJuegos.app.dtos.response.GeneroResponse;

public interface GeneroService {

	public void addGenero (GeneroRequest gen);
	public GeneroResponse getGenero(String genero);
}
