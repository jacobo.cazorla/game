package com.casa.tiendaJuegos.app.services.Impl;

import com.casa.tiendaJuegos.app.dtos.request.GameRequest;
import com.casa.tiendaJuegos.app.dtos.response.GameResponse;
import com.casa.tiendaJuegos.app.entities.Game;
import com.casa.tiendaJuegos.app.entities.Genero;
import com.casa.tiendaJuegos.app.helper.JuegoHelper;
import com.casa.tiendaJuegos.app.repository.GameRepository;
import com.casa.tiendaJuegos.app.services.GeneroService;
import com.casa.tiendaJuegos.app.services.JuegoService;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

@Service
public class JuegoServiceImpl implements JuegoService {

	@Autowired
	private GameRepository gameRepo;
	
	@Autowired
	JuegoHelper juegoHelper;
	
	@Autowired
	ConversionService converter;
	
	@Autowired
	GeneroService generoService;

	@Override
	public GameResponse addJuego(GameRequest juego) {
	
		//Aplicar los converters
		Game game = converter.convert(juego,Game.class);
		List<Genero> generos = juego.getGenero().stream().map(g->converter.convert(generoService.getGenero(g),Genero.class)).collect(Collectors.toList());

		game.setGeneros(generos);		
		
		//Guardar en base de datos
		gameRepo.save(game);
		
		//Preparar la respuesta
		GameResponse response = converter.convert(game, GameResponse.class);
		List<String> generosEnum =game.getGeneros().stream().map(g->g.getDescription()).collect(Collectors.toList());
		response.setGenero(generosEnum);
		
		return response;
		
	}

	@Override
	public GameResponse getJuego(String titulo) {
		//Buscar el juego por el título
		Game game = juegoHelper.getIfExistsGame(gameRepo.findByTitle(titulo));

		//Preparar la respuesta
		GameResponse response = converter.convert(game,GameResponse.class);
		List<String> generosEnum =game.getGeneros().stream().map(g->g.getDescription()).collect(Collectors.toList());
		response.setGenero(generosEnum);

		return response;
	}

	@Override
	public void deleteJuego(String titulo) {
		Game game = juegoHelper.getIfExistsGame(gameRepo.findByTitle(titulo));
		gameRepo.delete(game);
		
	}

	@Override
	public GameResponse updateJuego(GameRequest juego, String titulo) {
		
		//Aplicar los converters
		Game game = converter.convert(juego,Game.class);
		List<Genero> generos = juego.getGenero().stream().map(g->converter.convert(generoService.getGenero(g),Genero.class)).collect(Collectors.toList());

		game.setGeneros(generos);	
		
		Game gameActual = juegoHelper.getIfExistsGame(gameRepo.findByTitle(titulo));
		
		game.setId(gameActual.getId());
		
		gameRepo.save(game);
		
		//Preparar la respuesta
		GameResponse response = converter.convert(game, GameResponse.class);
		List<String> generosEnum =game.getGeneros().stream().map(g->g.getDescription()).collect(Collectors.toList());
		response.setGenero(generosEnum);
		
		return response;
	}

}
