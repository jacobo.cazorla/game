package com.casa.tiendaJuegos.app.services;

import com.casa.tiendaJuegos.app.dtos.request.ShopRequest;
import com.casa.tiendaJuegos.app.dtos.response.ShopResponse;

public interface ShopService {

	public ShopResponse addShop (ShopRequest shop);
	public ShopResponse getShop(Long id);

}
