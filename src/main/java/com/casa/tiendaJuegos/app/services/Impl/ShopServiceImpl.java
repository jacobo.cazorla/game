package com.casa.tiendaJuegos.app.services.Impl;

import com.casa.tiendaJuegos.app.dtos.request.ShopRequest;
import com.casa.tiendaJuegos.app.dtos.response.ShopResponse;
import com.casa.tiendaJuegos.app.entities.Shop;
import com.casa.tiendaJuegos.app.helper.ShopHelper;
import com.casa.tiendaJuegos.app.repository.ShopRepository;
import com.casa.tiendaJuegos.app.services.ShopService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

@Service
public class ShopServiceImpl implements ShopService {

	@Autowired
	private ShopRepository shopRepo;
	
	@Autowired
	ShopHelper shopHelper;
	
	@Autowired
	ConversionService converter;
	
	@Override
	public ShopResponse addShop(ShopRequest shopDto) {
	
		//Aplicar los converters
		Shop shop = converter.convert(shopDto,Shop.class);		
		
		//Guardar en base de datos
		shopRepo.save(shop);
		
		//Preparar la respuesta
		ShopResponse response = converter.convert(shop, ShopResponse.class);
		
		return response;		
	}

	@Override
	public ShopResponse getShop(Long id) {
		//Buscar el juego por el título
		Shop shop = shopHelper.getIfExistsShop(shopRepo.findById(id).get());

		//Preparar la respuesta
		ShopResponse response = converter.convert(shop,ShopResponse.class);

		return response;
	}
}
