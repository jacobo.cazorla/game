package com.casa.tiendaJuegos.app.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.casa.tiendaJuegos.app.entities.Stock;
import com.casa.tiendaJuegos.app.entities.Tienda_Game_Stock;


@Repository
public interface Tienda_Game_StockRepository extends JpaRepository<Tienda_Game_Stock, Long>{

	@Query("SELECT g FROM Tienda_Game_Stock g WHERE g.stock=?1")
	public Tienda_Game_Stock findByStock(Stock stock);
}
