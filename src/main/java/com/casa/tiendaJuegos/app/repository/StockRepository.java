package com.casa.tiendaJuegos.app.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.casa.tiendaJuegos.app.entities.Stock;


@Repository
public interface StockRepository extends JpaRepository<Stock, Long>{

	@Query("SELECT g FROM Stock g WHERE g.codigo=?1")
	public Stock findByCodigo(String codigo);
}
