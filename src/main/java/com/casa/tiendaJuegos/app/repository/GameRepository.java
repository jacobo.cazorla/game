package com.casa.tiendaJuegos.app.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.casa.tiendaJuegos.app.entities.Game;


@Repository
public interface GameRepository extends JpaRepository<Game, Long>{
	
	@Query("SELECT g FROM Game g WHERE g.title=?1")
	public Game findByTitle(String title);
}
