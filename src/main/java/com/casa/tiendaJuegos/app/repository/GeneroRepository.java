package com.casa.tiendaJuegos.app.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.casa.tiendaJuegos.app.entities.Genero;


@Repository
public interface GeneroRepository extends JpaRepository<Genero, Long>{
	
	@Query("SELECT g FROM Genero g WHERE g.description=?1")
	public Genero findByDescription(String descripcion);
}
