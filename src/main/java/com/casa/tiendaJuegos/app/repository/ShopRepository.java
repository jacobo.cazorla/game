package com.casa.tiendaJuegos.app.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.casa.tiendaJuegos.app.entities.Shop;


@Repository
public interface ShopRepository extends JpaRepository<Shop, Long>{

}
