package com.casa.tiendaJuegos.app.converter;


import org.springframework.core.convert.converter.Converter;

import com.casa.tiendaJuegos.app.dtos.response.StockResponse;
import com.casa.tiendaJuegos.app.entities.Stock;

public class StockEntityToStockDtoConverter implements Converter<Stock,StockResponse> {

	@Override
	public StockResponse convert(Stock stock) {
		StockResponse stockResponse = new StockResponse();
		
		stockResponse.setCodigo(stock.getCodigo());
		stockResponse.setFecha_entrada(stock.getFecha());
		
		return stockResponse;
	}
}
