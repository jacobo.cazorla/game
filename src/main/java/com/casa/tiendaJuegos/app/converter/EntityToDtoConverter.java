package com.casa.tiendaJuegos.app.converter;

import org.springframework.core.convert.converter.Converter;

import com.casa.tiendaJuegos.app.dtos.response.GameResponse;
import com.casa.tiendaJuegos.app.entities.Game;


public class EntityToDtoConverter implements Converter<Game,GameResponse> {

	@Override
	public GameResponse convert(Game game) {
		GameResponse gameDto = new GameResponse();
		gameDto.setTitulo(game.getTitle());
		gameDto.setFecha(game.getRelease());
		gameDto.setDescripcion(game.getDescription());		
		
		return gameDto;
	}

}
