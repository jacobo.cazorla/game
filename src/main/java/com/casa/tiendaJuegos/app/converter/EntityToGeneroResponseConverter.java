package com.casa.tiendaJuegos.app.converter;


import org.springframework.core.convert.converter.Converter;
import com.casa.tiendaJuegos.app.dtos.response.GeneroResponse;
import com.casa.tiendaJuegos.app.entities.Genero;

public class EntityToGeneroResponseConverter implements Converter<Genero,GeneroResponse> {

	@Override
	public GeneroResponse convert(Genero source) {
		GeneroResponse gen = new GeneroResponse();
		gen.setDescripcion(source.getDescription());
		gen.setId(source.getId());
		return gen;
	}
	
}
