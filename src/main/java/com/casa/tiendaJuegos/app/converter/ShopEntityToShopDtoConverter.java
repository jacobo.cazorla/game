package com.casa.tiendaJuegos.app.converter;

import org.springframework.core.convert.converter.Converter;

import com.casa.tiendaJuegos.app.dtos.response.ShopResponse;
import com.casa.tiendaJuegos.app.entities.Shop;


public class ShopEntityToShopDtoConverter implements Converter<Shop,ShopResponse> {

	@Override
	public ShopResponse convert(Shop shop) {
		ShopResponse shopResponse = new ShopResponse();
		
		shopResponse.setId(shop.getId());
		shopResponse.setDireccion(shop.getDireccion());
		
		return shopResponse;
	}

}
