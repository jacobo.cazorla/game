package com.casa.tiendaJuegos.app.converter;


import org.springframework.core.convert.converter.Converter;

import com.casa.tiendaJuegos.app.dtos.request.ShopRequest;
import com.casa.tiendaJuegos.app.entities.Shop;

public class ShopDtoToShopEntityConverter implements Converter<ShopRequest,Shop> {

	@Override
	public Shop convert(ShopRequest shopDto) {
		Shop shop = new Shop();
		
		shop.setId(shopDto.getId());
		shop.setDireccion(shopDto.getDireccion());
		
		return shop;
	}
	
}
