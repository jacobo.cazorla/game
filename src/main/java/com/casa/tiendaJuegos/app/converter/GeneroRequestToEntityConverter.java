package com.casa.tiendaJuegos.app.converter;


import org.springframework.core.convert.converter.Converter;

import com.casa.tiendaJuegos.app.dtos.request.GeneroRequest;
import com.casa.tiendaJuegos.app.entities.Genero;

public class GeneroRequestToEntityConverter implements Converter<GeneroRequest,Genero> {

	@Override
	public Genero convert(GeneroRequest source) {
		Genero gen = new Genero();
		gen.setDescription(source.getDescripcion());
		return gen;
	}
	
}
