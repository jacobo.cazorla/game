package com.casa.tiendaJuegos.app.converter;

import org.springframework.core.convert.converter.Converter;

import com.casa.tiendaJuegos.app.entities.Genero;
import com.casa.tiendaJuegos.app.enums.Generos;

public class FromEnumConverter implements Converter<Generos,Genero> {

	@Override
	public Genero convert(Generos gen) {
		Genero gene = new Genero();
		gene.setDescription(gen.toString());
		return gene;
	}
}
