package com.casa.tiendaJuegos.app.converter;


import org.springframework.core.convert.converter.Converter;

import com.casa.tiendaJuegos.app.dtos.request.StockRequest;
import com.casa.tiendaJuegos.app.entities.Stock;

public class StockDtoToStockEntityConverter implements Converter<StockRequest,Stock> {

	@Override
	public Stock convert(StockRequest stockDto) {
		Stock stock = new Stock();
		
		stock.setCodigo(stockDto.getCodigo());
		stock.setFecha(stockDto.getFecha_entrada());
		
		return stock;
	}
	
}
