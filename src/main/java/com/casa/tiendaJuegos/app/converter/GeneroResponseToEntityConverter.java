package com.casa.tiendaJuegos.app.converter;


import org.springframework.core.convert.converter.Converter;
import com.casa.tiendaJuegos.app.dtos.response.GeneroResponse;
import com.casa.tiendaJuegos.app.entities.Genero;

public class GeneroResponseToEntityConverter implements Converter<GeneroResponse,Genero> {

	@Override
	public Genero convert(GeneroResponse source) {
		Genero gen = new Genero();
		gen.setDescription(source.getDescripcion());
		gen.setId(source.getId());
		return gen;
	}
	
}
