package com.casa.tiendaJuegos.app.converter;


import org.springframework.core.convert.converter.Converter;

import com.casa.tiendaJuegos.app.dtos.request.GameRequest;
import com.casa.tiendaJuegos.app.entities.Game;

public class DtoToEntityConverter implements Converter<GameRequest,Game> {

	@Override
	public Game convert(GameRequest gameDto) {
		Game game = new Game();
		game.setTitle(gameDto.getTitulo());
		game.setRelease(gameDto.getFecha());
		game.setDescription(gameDto.getDescripcion());
		
		return game;
	}
	
}
