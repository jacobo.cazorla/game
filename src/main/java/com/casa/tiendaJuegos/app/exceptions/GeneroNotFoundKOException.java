package com.casa.tiendaJuegos.app.exceptions;

import com.casa.tiendaJuegos.app.exceptions.generic.GeneroKOException;

public class GeneroNotFoundKOException extends GeneroKOException{

	private static final long serialVersionUID = 1L;
	
	private static final String DETAIL = "Genero no encontrado";
	
	
	public GeneroNotFoundKOException(String detalle) {
		super(detalle);
	}

	public GeneroNotFoundKOException() {
		super(DETAIL);
	}
}
