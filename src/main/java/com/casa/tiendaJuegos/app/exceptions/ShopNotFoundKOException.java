package com.casa.tiendaJuegos.app.exceptions;

import com.casa.tiendaJuegos.app.exceptions.generic.ShopKOException;

public class ShopNotFoundKOException extends ShopKOException{

	private static final long serialVersionUID = 1L;
	
	private static final String DETAIL = "Tienda no encontrada";
	
	
	public ShopNotFoundKOException(String detalle) {
		super(detalle);
	}

	public ShopNotFoundKOException() {
		super(DETAIL);
	}
}
