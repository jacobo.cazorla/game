package com.casa.tiendaJuegos.app.exceptions.generic;

public class GeneroKOException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	private final String detalle;
	
	public GeneroKOException(String detalle) {
		this.detalle = detalle;
	}

	public String getDetalle() {
		return detalle;
	}	
}
