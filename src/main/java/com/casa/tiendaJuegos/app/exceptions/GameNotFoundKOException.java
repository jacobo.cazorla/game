package com.casa.tiendaJuegos.app.exceptions;

import com.casa.tiendaJuegos.app.exceptions.generic.GameKOException;

public class GameNotFoundKOException extends GameKOException{

	private static final long serialVersionUID = 1L;
	
	private static final String DETAIL = "Juego no encontrado";
	
	
	public GameNotFoundKOException(String detalle) {
		super(detalle);
	}

	public GameNotFoundKOException() {
		super(DETAIL);
	}
}
