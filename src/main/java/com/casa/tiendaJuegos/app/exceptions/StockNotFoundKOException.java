package com.casa.tiendaJuegos.app.exceptions;

import com.casa.tiendaJuegos.app.exceptions.generic.StockKOException;

public class StockNotFoundKOException extends StockKOException{

	private static final long serialVersionUID = 1L;
	
	private static final String DETAIL = "Stock no encontrado";
	
	
	public StockNotFoundKOException(String detalle) {
		super(detalle);
	}

	public StockNotFoundKOException() {
		super(DETAIL);
	}
}
