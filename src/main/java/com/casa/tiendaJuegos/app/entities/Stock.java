package com.casa.tiendaJuegos.app.entities;

import java.util.Date;

import javax.persistence.*;

import lombok.Data;

@Entity
@Table(name = "STOCKS")
@Data
public class Stock {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "CODIGO")
	private String codigo;
	
	@Column(name = "FECHA_ENTRADA")
	private Date fecha;
	
}
