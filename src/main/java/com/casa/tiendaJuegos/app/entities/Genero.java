package com.casa.tiendaJuegos.app.entities;

import java.util.List;

import javax.persistence.*;

import lombok.Data;

@Entity
@Table(name = "GENEROS")
@Data
public class Genero {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "DESCRIPTION")
	private String description;
	
	
    @ManyToMany(mappedBy = "generos", fetch = FetchType.LAZY)
    private List<Game> juegos;	

}
