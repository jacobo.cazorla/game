package com.casa.tiendaJuegos.app.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

import lombok.Data;

@Entity
@Table(name = "GAMES")
@Data
public class Game {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "TITLE")
	private String title;
	
	@Column(name = "DESCRIPTION")
	private String description;
	
	@Column(name = "DATE_RELEASE")
	private Date release;
	
    @ManyToMany(fetch = FetchType.LAZY,cascade=CascadeType.REMOVE)
    private List<Genero> generos;
    
    @OneToMany
    List<Stock> stock;
}
