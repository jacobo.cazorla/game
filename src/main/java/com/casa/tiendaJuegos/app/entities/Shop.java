package com.casa.tiendaJuegos.app.entities;

import java.util.List;

import javax.persistence.*;

import lombok.Data;

@Entity
@Table(name = "SHOPS")
@Data
public class Shop {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "DIRECCION")
	private String direccion;
	
    @OneToMany
    List<Stock> stock;
    
}
