package com.casa.tiendaJuegos.app.entities;

import javax.persistence.*;

import lombok.Data;

@Entity
@Table(name = "SHOPS_GAMES_STOCKS")
@Data
public class Tienda_Game_Stock {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "COD_SHOP")
	private Shop shop;
	
	@ManyToOne
	@JoinColumn(name = "COD_GAME")
	private Game game;
	
	@ManyToOne (cascade=CascadeType.REMOVE)
	@JoinColumn(name = "COD_STOCK")
	private Stock stock;
}
