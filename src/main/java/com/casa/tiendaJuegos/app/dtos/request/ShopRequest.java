package com.casa.tiendaJuegos.app.dtos.request;

import lombok.Data;

@Data
public class ShopRequest {
	private String direccion;
	private Long id;
}
