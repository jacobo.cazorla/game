package com.casa.tiendaJuegos.app.dtos.request;

import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class GameRequest {
	private String titulo;
	private String descripcion;
	private List<String> genero;
	private Date fecha;

}
