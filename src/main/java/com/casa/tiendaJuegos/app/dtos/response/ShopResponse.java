package com.casa.tiendaJuegos.app.dtos.response;

import lombok.Data;

@Data
public class ShopResponse {
	private String direccion;
	private Long id;
}
