package com.casa.tiendaJuegos.app.dtos.response;

import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class GameResponse {

	private String titulo;
	private String descripcion;
	private List<String> genero;
	private Date fecha;
	
	public GameResponse(){
		
	}
}
