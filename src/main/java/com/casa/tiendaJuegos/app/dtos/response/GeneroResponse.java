package com.casa.tiendaJuegos.app.dtos.response;

import lombok.Data;

@Data
public class GeneroResponse {

	private String descripcion;
	private Long id;
	
	public GeneroResponse(){
		
	}
}
