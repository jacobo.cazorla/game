package com.casa.tiendaJuegos.app.dtos;

import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.casa.tiendaJuegos.app.TiendaJuegosApplication;
import com.casa.tiendaJuegos.app.dtos.request.GeneroRequest;
import com.casa.tiendaJuegos.app.exceptions.GeneroNotFoundKOException;
import com.casa.tiendaJuegos.app.services.GeneroService;

@Component
public class CommandLineAppStartupRunner implements CommandLineRunner {
	
	@Autowired
	GeneroService generoService;

	@Override
	public void run(String... args) throws Exception {
		InputStream in = TiendaJuegosApplication.class.getClassLoader()
	            .getResourceAsStream("generos.xml");	
	
	DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory
            .newInstance();
	try {
        DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
     // by sending input stream as input to DOM
             Document doc = docBuilder.parse(in);	
     
             doc.getDocumentElement().normalize();  
             NodeList nodeList = doc.getElementsByTagName("genero");  
             
             // nodeList is not iterable, so we are using for loop  
             for (int itr = 0; itr < nodeList.getLength(); itr++)   
             {
            	 Node node = nodeList.item(itr); 
            	 String genero = node.getTextContent();
            	 
            	 try {
            		 generoService.getGenero(genero);
            	 } catch(GeneroNotFoundKOException e) {
            		 GeneroRequest generoRequest = new GeneroRequest();
            		 generoRequest.setDescripcion(genero);
            		generoService.addGenero(generoRequest); 
            	 } catch (Exception exx){
            		 System.out.println("Se ha producido una excepción: "+exx.getStackTrace());
            		 continue;
            	 }

             }
	} catch(Exception e) {
		System.out.println(e.getMessage());
	}


	}

}
