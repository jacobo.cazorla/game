package com.casa.tiendaJuegos.app.dtos.response;

import java.util.Date;

import lombok.Data;

@Data
public class StockResponse {
	private String codigo;
	private Date fecha_entrada;
	private Long id_shop;
	private Long id_game;
}
