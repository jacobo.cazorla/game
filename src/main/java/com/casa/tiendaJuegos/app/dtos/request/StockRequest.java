package com.casa.tiendaJuegos.app.dtos.request;

import java.util.Date;

import lombok.Data;

@Data
public class StockRequest {
	private String codigo;
	private Date fecha_entrada;
	private Long id_shop;
	private Long id_game;
}
