package com.casa.tiendaJuegos.app.dtos.request;

import lombok.Data;

@Data
public class GeneroRequest {

	private String descripcion;
	
	public GeneroRequest(){
		
	}
}
