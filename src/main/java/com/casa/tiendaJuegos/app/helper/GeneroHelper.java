package com.casa.tiendaJuegos.app.helper;

import org.springframework.stereotype.Service;

import com.casa.tiendaJuegos.app.entities.Genero;
import com.casa.tiendaJuegos.app.exceptions.GeneroNotFoundKOException;

@Service
public class GeneroHelper {
	
	public Genero getIfExistsGenero(Genero generoDto){
	
		if(generoDto != null)
			return generoDto;
		else
			throw new GeneroNotFoundKOException();
	}
}
