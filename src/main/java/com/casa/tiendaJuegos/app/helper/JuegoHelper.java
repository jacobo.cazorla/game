package com.casa.tiendaJuegos.app.helper;

import org.springframework.stereotype.Service;

import com.casa.tiendaJuegos.app.entities.Game;
import com.casa.tiendaJuegos.app.exceptions.GameNotFoundKOException;

@Service
public class JuegoHelper {
	
	public Game getIfExistsGame(Game gameDto){
	
		if(gameDto != null)
			return gameDto;
		else
			throw new GameNotFoundKOException();
	}
}
