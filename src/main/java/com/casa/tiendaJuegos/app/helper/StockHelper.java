package com.casa.tiendaJuegos.app.helper;

import org.springframework.stereotype.Service;

import com.casa.tiendaJuegos.app.entities.Stock;
import com.casa.tiendaJuegos.app.exceptions.StockNotFoundKOException;

@Service
public class StockHelper {
	
	public Stock getIfExistsStock(Stock stockDto){
	
		if(stockDto != null)
			return stockDto;
		else
			throw new StockNotFoundKOException();
	}
}
