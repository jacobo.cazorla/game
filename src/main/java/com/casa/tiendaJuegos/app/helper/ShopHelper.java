package com.casa.tiendaJuegos.app.helper;

import org.springframework.stereotype.Service;

import com.casa.tiendaJuegos.app.entities.Shop;
import com.casa.tiendaJuegos.app.exceptions.ShopNotFoundKOException;

@Service
public class ShopHelper {
	
	public Shop getIfExistsShop(Shop shopDto){
	
		if(shopDto != null)
			return shopDto;
		else
			throw new ShopNotFoundKOException();
	}
}
